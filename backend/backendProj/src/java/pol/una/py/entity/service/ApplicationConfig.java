/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.entity.service;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author victors
 */
@ApplicationPath("services")
public class ApplicationConfig extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(pol.una.py.entity.service.ActividadesFacadeREST.class);
        resources.add(pol.una.py.entity.service.RolesFacadeREST.class);
        resources.add(pol.una.py.entity.service.UsuariosFacadeREST.class);
        
    }    
    
    
}
