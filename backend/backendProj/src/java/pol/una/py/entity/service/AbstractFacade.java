/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pol.una.py.entity.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import pol.una.py.entity.Usuarios;

/**
 *
 * @author victors
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();
    

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public Usuarios login (Usuarios entity){
        Query query = getEntityManager().createNamedQuery("Usuarios.findByNombreyContrasena");
        query.setParameter("nombre", entity.getNombre());
        query.setParameter("contrasena", entity.getContrasena());
        List<Usuarios> listaUsuarios = query.getResultList();
        if (!listaUsuarios.isEmpty()) {
            return listaUsuarios.get(0);
        }
        System.out.println("No se encontro ningun Usuario registrado");
        return null;
    }
    
    public Usuarios LoginGoogle(String email){
        Query query = getEntityManager().createNamedQuery("Usuarios.findByEmail");
        query.setParameter("email", email);
        List<Usuarios> listaUsuarios2 = query.getResultList();
        if (!listaUsuarios2.isEmpty()) {
            return listaUsuarios2.get(0);
        }
        System.out.println("No se encontro ningun Usuario registrado con ese correo");
        return null;
    }
}
