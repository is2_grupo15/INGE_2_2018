BACKLOG

	Administración de Usuarios

	Creación de usuarios

RF-1 La aplicacion por defecto tendrá un usuario administrador que tendrá ciertos permisos asociados.
RF-2 La aplicacion permitirá crear usuarios al administrador que tenga el rol correspondiente para hacerlo.
RF-3 La aplicacion permitirá registrar nuevos usuarios.
RF-4 La aplicacion mostrará un formulario para la creación de usuarios.
RF-5 Para crear usuarios se deben completar los campos:
	RF-5.1 Nombre de Usuario
RF-5.2 Nombres
	RF-5.3 Apellidos
	RF-5.4 Email
	RF-5.5 Contraseña
	RF-5.6 Teléfono.
RF-6 La aplicacion permitirá seleccionar el perfil de usuario.	
RF-7 La aplicacion registrará al usuario, la fecha y hora actual de creación.

	Modificación de Usuarios

RF-8 La aplicacion permitirá al usuario el cual tenga el rol de administrador poder modificar los datos de cualquier usuario.
RF-9 La aplicacion permitirá que un usuario pueda modificar su propia contraseña.
RF-10 La aplicacion proporcionará la opción de restauración de contraseña en caso  de que el usuario no lo recuerde.
RF-11 La aplicacion actualiza la información del usuario con los datos proporcionados
RF-12 La aplicacion registrará el usuario, la fecha y hora de la modificación realizada.

	Eliminación de Usuarios

RF-13 La aplicacion permitirá eliminar cuentas del usuario
RF-14 La aplicacion registrará automáticamente el Usuario, la fecha y hora de la eliminación del Usuario.
RF-15 La aplicacion permitirá la restauración de un usuario en caso de que haya sido eliminado por error.

	Autenticación de Usuario

RF-16 La aplicacion permitirá el acceso a usuarios verificando que el nombre de usuario y la contraseña sean válidos.   
RF-17 La aplicacion proporcionará al usuario cerrar sesión cuando éste así lo desee.
RF-18 La aplicacion registra el usuario, la fecha y hora en el que inicia y finaliza su sesión.

	Administración de Roles

	Creación del Rol

RF-19 La aplicacion permitirá a un usuario (con los permisos co-rrespondientes) crear un rol.
RF-20 De los roles se almacenarán un nombre, descripción, entidades asociadas y conjunto de permisos asignados al rol.
RF-21 El usuario debe seleccionar al menos un permiso para asignar al rol.
RF-22 La aplicacion registra el usuario, la fecha y la hora en la que se creó el rol.

	Modificación del Rol

RF-23 La aplicacion permitirá modificar todos los datos de un Rol creados por el Usuario.
RF-24 La aplicacion registrará automáticamente el Usuario, la fe-cha y hora de la modificación del Rol.

	Eliminación del Rol

 RF-25 La aplicacion permitirá eliminar los Roles creados por el Usuario.
 RF-26 La aplicacion solicitará una confirmación al Usuario antes de eliminar el Rol.
 RF-27 La aplicacion registra automáticamente el usuario, la fecha y la hora de la eliminación del rol.

	Permisos

RF-28 Los permisos estarán definidos en La aplicacion
RF-29  Los permisos no podrán ser eliminados ni modificados.

	Administración de proyectos

	Creación de proyectos

RF-30 Los usuarios que posean el permiso “Crear Proyecto” podrán crear y gestionar proyectos.
RF-31 La aplicacion deberá almacenar los siguientes datos relati-vos al proyecto: código, nombre, fecha de inicio, fecha de fin estimada.
RF-32 El proyecto deberá estar asociado a uno o más clientes
RF-33 Los usuarios pueden ser miembros de diversos equipos de proyectos
RF-34 La aplicacion permitirá asignar un líder al proyecto, al que posea los permisos correspondientes.
RF-35 La aplicacion permitirá establecer estados a los proyectos:
RF-35.1 Planificado
RF-35.2 Desarrollo
RF-35.3 Resuelto
RF-36 La aplicacion registra el Usuario, la fecha y la hora de la creación del proyecto.

	Modificación  y eliminación de Proyectos

RF-37 La aplicacion permitirá la modificación  de los campos de un proyecto a excepción del nombre y el código.
RF-38 La aplicacion permitirá modificar el estado de un proyecto si el usuario cuenta con los permisos correspondientes.
RF-39 Un proyecto Resuelto no podrá ser modificado.
RF-40 La aplicacion registra el Usuario, la fecha y la hora de la modificación del proyecto.
RF-41 La aplicacion permitirá la eliminación de proyectos.

	Equipo(Team)

RF-42 El líder, mediante La aplicacion podrá:
	RF-42.1 Asignar miembros al proyecto.
	RF-42.2 Asignar roles y permisos a los miembros del proyecto.

	Sprint

RF-43 La aplicacion debe permitir definir los sprint durante el proyecto en cambio la duración de los mismos tendrá una duración fija.
RF-44 La cantidad de Sprint por proyecto se generará de acuerdo a la duración establecido por el cliente
RF-45 La aplicacion registrará el usuario, la hora y la fecha de la crea-ción del sprint.


	Administración de Flujos

	Creación de flujos

RF-46 Se deberá poder definir los flujos de kamban
RF-47 Se deberá poder definir al menos un flujo o más por ca-da proyecto
RF-48 La estructura del kamban deberá ser definida por el usuario en bases a fases y estados
RF-49 La aplicacion registra el usuario, la fecha y la hora en el que se creó un flujo.	

	Eliminación/Modificación de flujos

RF-50 La aplicacion permite a los usuarios con permisos corres-pondientes eliminar o modificar flujos que no estén relaciona-dos a ningún  proyecto.
RF-51 La aplicacion registra el usuario, la fecha y la hora en el que se eliminó o modifico un flujo.

	Administración de Actividades

	Creación de actividades

RF-52 La aplicacion permitirá la definición de actividades.
RF-53 La aplicacion almacenará para cada actividad: un nombre, descripción y estados.
RF-53.1 Proporcionar el nombre de la actividad es obligatorio.

	Modificación y Eliminación de actividades

RF-54 La aplicacion permitirá la modificación de las actividades.
	RF-54.1 Se podrán modificar los atributos nombre y descripción
RF-55 La aplicacion permitirá la eliminación de la actividad siempre que dicha actividad no pertenezca a un proyecto activo.

	Estados de las actividades:

RF-56 Una actividad puede tener los estados “pendiente”, “en curso”, “culminado”:
	RF-56.1 Cada estado posee una lista de user stories. 
	RF-56.2 “pendiente” denota que el user stories está listo para realizarse.
	RF-56.3 “en curso” denota que el user stories está sien-do realizado por algún usuario.
	RF-56.4 “Culminado” denota que el user stories está fi-nalizado.

	Administración de User Stories

	Creación de user stories

RF-57 La aplicacion permitirá el registro de los user stories una vez iniciado el / los proyecto /s
RF-58 La aplicacion permitirá la creación de user stories con las siguientes propiedades:
RF-58.1 Nombre corto
RF-58.2 Nombre largo
RF-58.3 Numeración automática generada por el siste-ma
RF-58.4 Descripción
RF-58.5 Valor del negocio (0-10)
RF-58.6 valor técnico (0-10)
RF-59 La aplicacion permitirá asignar user stories a sprint
RF-60 La aplicacion permitirá registrar la cantidad de días  esti-mada para el user stories
RF-61 Los user stories  tendrán los siguientes campos opciona-les:
	RF-61.1 Historial
		RF-61.1 Agregar notas
		RF-61.2 Asignación de  flujos
		RF-61.3 Archivos adjuntos
RF-62 La aplicacion tendrá un registro de las horas acumuladas por user stories.
	
	Modificación de user stories

RF-63 La aplicacion permitirá la modificación de los user stories.
	RF-63.1 Se podrán modificar los estados dentro de las diversas fases del flujo.
RF-64 La aplicacion permitirá la eliminación de la actividad siempre que se tengan los permisos necesarios.

	Administración de User Stories

RF-65 La aplicacion deberá poder estar integrado a un servicio de correo electrónico
RF-66 La aplicacion notificará tanto al usuario creador como al usuario al que fue asignado el user stories, de las modificaciones, agregaciones o cambios realizados.

	Reporte (Burndown Chart)

RF-67 El Burndown Chart mostrará la relación de horas trabajadas por día entre lo planificado y lo dedicado con relación a cada Sprint.
RF-68 La aplicacion permite generar un gráfico según el avance de cada proyecto.
	RF-68.1 El Grafico se traza para cada sprint.
	RF-68.2 El eje x del grafico representa la cantidad de días del sprint. 
RF-68.3 El eje y del grafico representa las horas utilizadas.
RF-69 La aplicacion graficara la curva de planificación vs curva de eje-cución. 
RF-70 El usuario con los permisos correspondientes podrá visualizar el proceso del proyecto en comparación al burndown chart generado por La aplicacion.

